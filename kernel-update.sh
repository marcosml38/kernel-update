#!/usr/bin/env bash
# Copyright (C) 2019, Marcos Moreira <marcosml38@protonmail.ch>

# Repositório do kernel
kernelHeaders="https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.4.1/linux-headers-5.4.1-050401_5.4.1-050401.201911290555_all.deb"
kernelImage="https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.4.1/linux-image-unsigned-5.4.1-050401-generic_5.4.1-050401.201911290555_amd64.deb"
kernelModules="https://kernel.ubuntu.com/~kernel-ppa/mainline/v5.4.1/linux-modules-5.4.1-050401-generic_5.4.1-050401.201911290555_amd64.deb"

# Coloração de erro/sucesso
corVerde="\\e[0;42m"
corVermelha="\\e[0;41m"
corFinal="\\e[0m"

# Variáveis com comandos
pastaTemporaria=$(mktemp -d)
versaoKernel=$(uname -r)
checagemUbuntu=$(cat /etc/os-release | grep -i "ubuntu"; echo $?)
compararVersoes=$(uname -r | grep "5.4.1"; echo $?)

# Função de ajuda
function exibirAjuda()
{
	echo "kernel-ubuntu.sh [parâmetro]"
	echo "-i	Instala a versão recente do kernel."
	echo "-v	Exibe a versão atual do kernel."
}

# Função de saída
function testarSaida()
{
	if [[ "$?" == "0" ]]; then
		echo -e "${corVerde}Atualização finalizada. Reinicie para aplicar as alterações.${corFinal}"
	else
		echo -e "${corVermelha}Erro na atualização. Verifique o arquivo 'dpkg.log' em '/var/log/'.${corFinal}"
	fi
}

# Checagem de requisitos
if [[ $(id -u) -ne "0" ]]; then
	echo "É necessário acesso ao usuário 'root'."
	exit 1
elif [[ $(uname -m) == "i386" ]]; then
	echo "Arquitetura não suportada."
	exit 1
elif [[ ${UBUNTU} == "1" ]]; then
	echo "Distribuição não suportada."
	exit 1
elif ! [[ $(which wget) ]]; then
	echo "Utilitário 'wget' não encontrado."
	exit 1
elif [[ -z "$1" ]]; then
	exibirAjuda
	exit 1
fi

# Entrada de parâmetros
case "$1" in
	-i)
		if [[ ${compararVersoes} == "1" ]]; then
			wget -c -q --show-progress -P ${pastaTemporaria} ${kernelHeaders} ${kernelImage} ${kernelModules}
			dpkg -i ${pastaTemporaria}/*.deb
			testarSaida
			rm -rf ${pastaTemporaria}
		else
			echo "Não existem atualizações disponíveis."
		fi
	;;
	-v)
		echo "Versão atual do kernel: ${versaoKernel}"
	;;
	*)
		exibirAjuda
	;;
esac
